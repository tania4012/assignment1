use strict;
use warnings;

my $number= 600851475143;  
my $factor;
for ( $factor = 2; $factor <$number; $factor++ ) {
    next if $number % $factor;
    $number /= $factor;
  
    redo;
}
   print "The largest prime factor is " . $factor. " .\n" ;